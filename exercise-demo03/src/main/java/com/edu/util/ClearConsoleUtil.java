package com.edu.util;

import java.util.Scanner;

/**
 * @author five-five
 * @created 2021/12/6-17:02
 */
public class ClearConsoleUtil {
    private ClearConsoleUtil() {
    }

    public static void doClearConsole(Scanner in) {
        System.out.println("===================================请选择是否要进行清屏?[1：清屏]=====================================");
        if (in == null) {
            return;
        }
        if (in.nextInt() == 1) {
            clearConsole();
        } else {
            System.out.println("===================================未进行清屏操作===================================");
        }
    }

    public static void clearConsole() {
        try {
            final String os = System.getProperty("os.name");
            if (os.contains("Windows")) {
                Runtime.getRuntime().exec("cls");
            } else {
                Runtime.getRuntime().exec("clear");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            try {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
                System.out.println("==================================清屏失败===============================");
            }
        }
    }
}
