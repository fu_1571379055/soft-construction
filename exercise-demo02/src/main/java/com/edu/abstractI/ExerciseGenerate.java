package com.edu.abstractI;

import com.edu.pojo.Exercise;

import java.util.List;

/**
 * @author five-five
 * @created 2021/11/20 - 20:50
 */
public abstract class ExerciseGenerate {
    protected Symbol symbol;

    public ExerciseGenerate() {
    }

    public ExerciseGenerate(Symbol symbol) {
        this.symbol = symbol;
    }

    public List<Exercise> generateExercise() {
        //默认生成50个算式
        return generateExercise(50);
    }


    public abstract List<Exercise> generateExercise(int length);
}
