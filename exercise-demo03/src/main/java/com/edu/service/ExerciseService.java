package com.edu.service;

import com.edu.dao.BasiFormulacBase;
import com.edu.pojo.Exercise;

import java.util.ArrayList;
import java.util.List;

/**
 * @author five-five
 * @created 2021/11/26-17:17
 */
public class ExerciseService {
    /**
     * 生成加算式
     *
     * @param length 题目长度
     * @return
     */
    public List<Exercise> generateAddFormula(int length) {
        List<Exercise> exercises = new ArrayList<>();
        while (exercises.size() < length) {
            int x = (int) (Math.ceil(Math.random() * 101) - 1);
            int y = (int) (Math.ceil(Math.random() * 101) - 1);
            if (y + x < 100) {
                Exercise exercise = new Exercise(y, x, "+", BasiFormulacBase.generateAddBasiFormulacBase()[x][y]);
                if (!exercises.contains(exercise)) {
                    exercises.add(exercise);
                }
            }
        }
        return exercises;
    }

    /**
     * 生成减算式
     *
     * @param length 题目长度
     * @return
     */
    public List<Exercise> generateSubtractFormula(int length) {
        List<Exercise> exercises = new ArrayList<>();
        while (exercises.size() < length) {
            int x = (int) (Math.ceil(Math.random() * 101) - 1);
            int y = (int) (Math.ceil(Math.random() * 101) - 1);
            if (y - x > 0) {
                Exercise exercise = new Exercise(y, x, "-", BasiFormulacBase.generateSubtractBasiFormulacBase()[x][y]);
                if (!exercises.contains(exercise)) {
                    exercises.add(exercise);
                }
            }
        }
        return exercises;
    }

    /**
     * 生成混合算式
     *
     * @param length 题目长度
     * @return
     */
    public List<Exercise> generateBinaryFormula(int length) {
        ArrayList<Exercise> exercises = new ArrayList<>();
        exercises.addAll(generateAddFormula(length / 2));
        exercises.addAll(generateSubtractFormula((length - length / 2)));
        return exercises;
    }
}
