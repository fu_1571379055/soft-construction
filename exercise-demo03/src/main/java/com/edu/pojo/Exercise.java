package com.edu.pojo;

/**
 * @author five-five
 * @created 2021/11/26-15:43
 */
public class Exercise {
    private int left;
    private int right;
    private String symbol;
    private int result;

    public Exercise() {
    }

    public Exercise(int left, int right, String symbol, int result) {
        this.left = left;
        this.right = right;
        this.symbol = symbol;
        this.result = result;
    }

    public String fullString() {
        return this.left + this.symbol + this.right + "=";
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
