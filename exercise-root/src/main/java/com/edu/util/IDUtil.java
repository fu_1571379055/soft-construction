package com.edu.util;

import cn.hutool.core.lang.ObjectId;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;

/**
 * @author five-five
 * @created 2021/11/8-19:59
 */
public class IDUtil {
    private static final Snowflake snowflake = IdUtil.createSnowflake(1, 1);

    public static String generatorSnowflakeID() {
        return snowflake.nextIdStr();
    }

    public static String generatorObjectIdID() {
        return ObjectId.next();
    }
}
