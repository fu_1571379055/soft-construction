package com.edu.mapper;

import com.edu.pojo.Excise;
import com.edu.pojo.ExciseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ExciseMapper {
    long countByExample(ExciseExample example);

    int deleteByExample(ExciseExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Excise record);

    int insertSelective(Excise record);

    List<Excise> selectByExample(ExciseExample example);

    Excise selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Excise record, @Param("example") ExciseExample example);

    int updateByExample(@Param("record") Excise record, @Param("example") ExciseExample example);

    int updateByPrimaryKeySelective(Excise record);

    int updateByPrimaryKey(Excise record);
}