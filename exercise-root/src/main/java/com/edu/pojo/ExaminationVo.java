package com.edu.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author five-five
 * @created 2021/11/15 - 14:20
 */
@ApiModel("用于修改试卷所需的数据抽象")
public class ExaminationVo {
    @ApiModelProperty("总题数")
    private int totalProblems;
    @ApiModelProperty("正确题数")
    private int rightProblems;
    @ApiModelProperty("错误题数")
    private int errorProblems;
    @ApiModelProperty("错误率")
    private double errorRate;
    @ApiModelProperty("正确率")
    private double rightRate;
    @ApiModelProperty("试卷id")
    private int currentExaminationId;

    public int getCurrentExaminationId() {
        return currentExaminationId;
    }

    public void setCurrentExaminationId(int currentExaminationId) {
        this.currentExaminationId = currentExaminationId;
    }

    public int getTotalProblems() {
        return totalProblems;
    }

    public void setTotalProblems(int totalProblems) {
        this.totalProblems = totalProblems;
    }

    public int getRightProblems() {
        return rightProblems;
    }

    public void setRightProblems(int rightProblems) {
        this.rightProblems = rightProblems;
    }

    public int getErrorProblems() {
        return errorProblems;
    }

    public void setErrorProblems(int errorProblems) {
        this.errorProblems = errorProblems;
    }

    public double getErrorRate() {
        return errorRate;
    }

    public void setErrorRate(double errorRate) {
        this.errorRate = errorRate;
    }

    public double getRightRate() {
        return rightRate;
    }

    public void setRightRate(double rightRate) {
        this.rightRate = rightRate;
    }
}
