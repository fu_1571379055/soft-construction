package com.edu.finalPo;

import com.edu.abstractI.Symbol;
import com.edu.pojo.Exercise;

/**
 * 算式基常量
 *
 * @author five-five
 * @created 2021/11/20 - 21:13
 */
public enum ExercisesBase {
    ;


    public static final int len = 101;
    private static final int[][] nums = new int[len][len];


    /**
     * 加算式
     *
     * @return new int[][]；加算式
     */
    public static int[][] getAddExercisesBaseBase() {
        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j < nums[i].length; j++) {
                if (i + j < 100) {
                    nums[i][j] = i + j;
                }
            }
        }
        return nums;
    }

    /**
     * 减算式
     *
     * @return new int[][]；减算式
     */
    public static int[][] getSubtractExercisesBaseBase() {
        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j < nums[i].length; j++) {
                if (j - i > 0) {
                    nums[i][j] = j - i;
                }
            }
        }
        return nums;
    }

    /**
     * 根据算式基转换成：Exercise[][]
     *
     * @param nums   数组
     * @param symbol 符号：必须是Symbol实现类
     * @return 根据算式基转换成：Exercise[][]
     */
    public static Exercise[][] getExercisesBaseBaseToArray(int[][] nums, Symbol symbol) {
        Exercise[][] exercises = new Exercise[nums.length][nums[0].length];
        for (int i = 1; i < nums.length; i++) {
            for (int j = 1; j < nums[i].length; j++) {
                Exercise exercise = new Exercise();
                exercise.setLeft(j);
                exercise.setRight(i);
                exercise.setSymbol(symbol);
                exercises[i][j] = exercise;
            }
        }
        return exercises;
    }
}
