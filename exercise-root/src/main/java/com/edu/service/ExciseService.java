package com.edu.service;

import com.edu.pojo.Excise;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author five-five
 * @created 2021/11/12 - 12:19
 */
public interface ExciseService {

    Excise[][] generateAddFormulaBase(HttpServletRequest request);

    Excise[][] generateSubtractFormulaBase(HttpServletRequest request);

    List<Excise> generateAddExcises(int length, HttpServletRequest request);

    List<Excise> generateSubtractExcises(int length, HttpServletRequest request);

    Map<String, List<Excise>> randomGenerateSubtractExcises(int maxlength, int length, HttpServletRequest request);
}
