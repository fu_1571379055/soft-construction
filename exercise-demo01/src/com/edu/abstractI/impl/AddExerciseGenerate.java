package com.edu.abstractI.impl;

import com.edu.abstractI.ExerciseGenerate;
import com.edu.abstractI.Symbol;
import com.edu.finalPo.ExercisesBase;
import com.edu.pojo.Exercise;

import java.util.ArrayList;
import java.util.List;

/**
 * 生成加算式
 *
 * @author five-five
 * @created 2021/11/20 - 21:10
 */
public class AddExerciseGenerate extends ExerciseGenerate {

    /**
     * @param symbol 必须是AddSymbolImpl
     */
    public AddExerciseGenerate(Symbol symbol) {
        super(symbol);
    }

    @Override
    public List<Exercise> generateExercise(int length) {
        ArrayList<Exercise> exercises = new ArrayList<>();
        while (exercises.size() < length) {
            int x = (int) Math.ceil(Math.random() * ExercisesBase.len) - 1;
            int y = (int) Math.ceil(Math.random() * ExercisesBase.len) - 1;
            Exercise[][] exercisesBaseBaseToList = ExercisesBase.getExercisesBaseBaseToArray(ExercisesBase.getAddExercisesBaseBase(), symbol);
            Exercise exercise = exercisesBaseBaseToList[x][y];
            if (!exercises.contains(exercise)) {
                exercises.add(exercise);
            }
        }
        return exercises;
    }
}
