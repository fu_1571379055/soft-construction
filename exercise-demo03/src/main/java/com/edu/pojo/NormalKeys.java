package com.edu.pojo;

import java.io.File;

/**
 * @author five-five
 * @created 2021/11/26-16:32
 */
public interface NormalKeys {
    String USERLIST = "user-list";
    String USEREXAMINATIONPOST = "_userexaminationpost";
    String ADD_BASICBASEFILEPATH = "jsons" + File.separator + "ADD_BASICBASEFILEPATH.json";
    String SUBTRACT_BASICBASEFILEPATH = "jsons" + File.separator + "SUBTRACT_BASICBASEFILEPATH.json";
    String TIMER_KEY="_TIMER_KEY";
}
