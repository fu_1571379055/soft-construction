package com.edu.Aware;

import cn.hutool.core.collection.CollectionUtil;
import com.edu.mapper.ExciseMapper;
import com.edu.pojo.AllKeyEnum;
import com.edu.pojo.Excise;
import com.edu.util.JsonUtil;
import com.edu.util.RedisClientUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 初始化soft-constructor的t_excise数据
 *
 * @author five-five
 * @created 2021/11/12 - 15:48
 */
@Component
public class SQLDataInitService implements InitializingBean {
    @Resource
    private ApplicationContext applicationContext;


    @Override
    public void afterPropertiesSet() throws Exception {
//        initSqlData();
//        System.out.println(1/0);
        initRedisData();
    }

    private void initRedisData() {
        RedisClientUtil redisClientUtil = applicationContext.getBean(RedisClientUtil.class);
        if (CollectionUtil.isEmpty(redisClientUtil.keys("prefix_excises_*"))) {
//            //开始初始化数据
            initAddFormulatoRedis(redisClientUtil);
//            //开始初始化数据
            initSubtractFormulatoRedis(redisClientUtil);
//            System.out.println("请重启redis，热加载。。。。");
        }
    }

    private void initAddFormulatoRedis(RedisClientUtil redisClientUtil) {
        int index = 1;
        int[][] nums = new int[101][101];
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums[i].length; j++) {
                if (i + j <= 100) {
                    Excise excise = new Excise();
                    excise.setLeft(j + "");
                    excise.setRight(i + "");
                    excise.setResult((i + j) + "");
                    excise.setSymbol("+");
                    redisClientUtil.set(AllKeyEnum.PREFIX_ADD_EXCISES.getKey() + index++, JsonUtil.objectToJson(excise));
                }
            }
        }
        redisClientUtil.set(AllKeyEnum.ADD_MAX_LENGTH.getKey(), index + "");
    }

    private void initSubtractFormulatoRedis(RedisClientUtil redisClientUtil) {
        int[][] nums = new int[101][101];
        int index = 1;
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums[i].length; j++) {
                if (j - i >= 0) {
                    Excise excise = new Excise();
                    excise.setLeft(j + "");
                    excise.setRight(i + "");
                    excise.setResult((j - i) + "");
                    excise.setSymbol("-");
                    redisClientUtil.set(AllKeyEnum.PREFIX_SUBTRACT_EXCISES.getKey() + index++, JsonUtil.objectToJson(excise));
                }
            }
        }
        redisClientUtil.set(AllKeyEnum.SUBTRACT_MAX_LENGTH.getKey(), index + "");
    }

    private void initSqlData() {
        ExciseMapper exciseMapper = applicationContext.getBean(ExciseMapper.class);
        if (!CollectionUtil.isEmpty(exciseMapper.selectByExample(null))) {
            return;
        }
        //开始初始化数据
        initAddFormulatoSql(exciseMapper);
        initSubtractFormulatoSql(exciseMapper);
    }


    /**
     * 初始化减算式
     *
     * @param exciseMapper dao层
     */
    private void initSubtractFormulatoSql(ExciseMapper exciseMapper) {
        int[][] nums = new int[101][101];
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums[i].length; j++) {
                if (j - i >= 0) {
                    Excise excise = new Excise();
                    excise.setLeft(j + "");
                    excise.setRight(i + "");
                    excise.setResult((j - i) + "");
                    excise.setSymbol("-");
                    exciseMapper.insertSelective(excise);
                }
            }
        }
    }

    /**
     * 初始化加算式
     *
     * @param exciseMapper dao层
     */
    private void initAddFormulatoSql(ExciseMapper exciseMapper) {
        int[][] nums = new int[101][101];
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums[i].length; j++) {
                if (i + j <= 100) {
                    Excise excise = new Excise();
                    excise.setLeft(j + "");
                    excise.setRight(i + "");
                    excise.setResult((i + j) + "");
                    excise.setSymbol("+");
                    exciseMapper.insertSelective(excise);
                }
            }
        }
    }
}
