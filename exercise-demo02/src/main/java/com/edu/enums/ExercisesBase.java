package com.edu.enums;

import com.edu.abstractI.Symbol;
import com.edu.abstractI.impl.AddSymbolImpl;
import com.edu.abstractI.impl.SubtractSymbolImpl;
import com.edu.pojo.Exercise;
import com.edu.util.JsonUtil;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * 算式基常量:第二版新增添加至json文件的功能
 *
 * @author five-five
 * @created 2021/11/20 - 21:13
 */
public enum ExercisesBase {
    ;


    public static final int len = 101;
    private static int[][] nums = new int[len][len];
    //加算式基文件路径
    public static final String ADD_FILE_PATH = "jsons" + File.separator + "add.json";
    //减算式基文件路径
    public static final String SUBTRACT_FILE_PATH = "jsons" + File.separator + "subtract.json";


    /**
     * 加算式
     *
     * @return new int[][]；加算式
     */
    public static int[][] getAddExercisesBase() {
        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j < nums[i].length; j++) {
                if (i + j < 100) {
                    nums[i][j] = i + j;
                }
            }
        }
        return nums;
    }

    /**
     * 减算式
     *
     * @return new int[][]；减算式
     */
    public static int[][] getSubtractExercisesBase() {
        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j < nums[i].length; j++) {
                if (j - i > 0) {
                    nums[i][j] = j - i;
                }
            }
        }
        return nums;
    }

    /**
     * 根据算式基转换成：Exercise[][]
     *
     * @param nums   数组
     * @param symbol 符号：必须是Symbol实现类
     * @return 根据算式基转换成：Exercise[][]
     */
    public static Exercise[][] getExercisesBaseBaseToArray(int[][] nums, Symbol symbol) {
        Exercise[][] exercises = new Exercise[nums.length][nums[0].length];
        for (int i = 1; i < nums.length; i++) {
            for (int j = 1; j < nums[i].length; j++) {
                Exercise exercise = new Exercise();
                exercise.setLeft(j);
                exercise.setRight(i);
                exercise.setSymbol(symbol);
                exercises[i][j] = exercise;
            }
        }
        return exercises;
    }

    /**
     * 把算式基添加至json文件中
     *
     * @param nums 算式基:二维数组
     * @param num  0:加算式 1:减算式
     * @return json文件
     */
    public static File jsonToFile(int[][] nums, int num) throws IOException {
        File file = null;
        if (num == 0) {
            file = new File(ADD_FILE_PATH);
        } else {
            file = new File(SUBTRACT_FILE_PATH);
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        String json = JsonUtil.objectToJson(nums);
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
        byte[] bytes = json.getBytes(StandardCharsets.UTF_8);
        bos.write(bytes, 0, bytes.length);
        bos.flush();
        bos.close();
        return file;
    }

    /**
     * @param num 0:加算式 1:减算式
     * @return Exercise[][]
     */
    public static int[][] getExercisesBaseByFile(int num) throws IOException {
        File file = null;
        if (num == 0) {
            file = new File(ADD_FILE_PATH);
        } else {
            file = new File(SUBTRACT_FILE_PATH);
        }
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
        StringBuilder stringBuilder = new StringBuilder();
        byte[] buffer = new byte[1024 * 1024];
        int len = -1;
        while ((len = bis.read(buffer, 0, buffer.length)) != -1) {
            stringBuilder.append(new String(buffer, StandardCharsets.UTF_8));
        }
        ;
        String jsonstr = stringBuilder.toString();
        nums = JsonUtil.jsonToPojo(jsonstr, int[][].class);
        return nums;
    }

    /**
     * 算式基
     *
     * @return
     */
    public static int[][] getExercisesBase() {
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums[i].length; j++) {
                if (i + j < 100) {
                    nums[i][j] = i + j;
                } else {
                    if (j - i > 0) {
                        nums[i][j] = j - i;
                    } else {
                        nums[i][j] = i - j;
                    }
                }
            }
        }
        return nums;
    }

    /**
     * Excise算式基
     *
     * @return
     */
    public static Exercise[][] getExerciseExercisesBase() {
        Exercise[][] exercises = new Exercise[len][len];
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums[i].length; j++) {
                Exercise exercise = new Exercise();
                if (i + j < 100) {
                    exercise.setLeft(j);
                    exercise.setRight(i);
                    exercise.setSymbol(new AddSymbolImpl());
                    nums[i][j] = i + j;
                    exercises[i][j]=exercise;
                } else {
                    if (j - i > 0) {
                        exercise.setLeft(j);
                        exercise.setRight(i);
                        exercise.setSymbol(new SubtractSymbolImpl());
                        nums[i][j] = j - i;
                        exercises[i][j]=exercise;
                    } else {
                        exercise.setLeft(i);
                        exercise.setRight(j);
                        exercise.setSymbol(new SubtractSymbolImpl());
                        nums[i][j] = i - j;
                        exercises[i][j]=exercise;
                    }
                }
            }
        }
        return exercises;
    }
}
