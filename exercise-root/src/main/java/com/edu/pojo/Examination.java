package com.edu.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * t-examination
 *
 * @author
 */
@ApiModel(value = "试卷类(保存试题)")
public class Examination implements Serializable {
    /**
     * 主键-自增
     */
    @ApiModelProperty(value = "主键-自增")
    private Integer id;

    /**
     * 试卷名
     */
    @ApiModelProperty(value = "试卷名")
    private String name;

    /**
     * 创建该试卷的用户唯一标识
     */
    @ApiModelProperty(value = "创建该试卷的用户唯一标识")
    private Integer createUser;

    /**
     * 记录该试卷的基本情况
     */
    @ApiModelProperty(value = "记录该试卷的基本情况")
    private ExaminationVo examinationVo;

    public ExaminationVo getExaminationVo() {
        return examinationVo;
    }

    public void setExaminationVo(ExaminationVo examinationVo) {
        this.examinationVo = examinationVo;
    }

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Integer createUser) {
        this.createUser = createUser;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Examination other = (Examination) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
                && (this.getCreateUser() == null ? other.getCreateUser() == null : this.getCreateUser().equals(other.getCreateUser()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getCreateUser() == null) ? 0 : getCreateUser().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", createUser=").append(createUser);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}