package com.edu.mapper;

import com.edu.pojo.UserExamina;
import com.edu.pojo.UserExaminaExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserExaminaMapper {
    long countByExample(UserExaminaExample example);

    int deleteByExample(UserExaminaExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(UserExamina record);

    int insertSelective(UserExamina record);

    List<UserExamina> selectByExample(UserExaminaExample example);

    UserExamina selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") UserExamina record, @Param("example") UserExaminaExample example);

    int updateByExample(@Param("record") UserExamina record, @Param("example") UserExaminaExample example);

    int updateByPrimaryKeySelective(UserExamina record);

    int updateByPrimaryKey(UserExamina record);
}