package com.edu.client;

import com.edu.builder.ExerciseBuilder;
import com.edu.enums.ExercisesBase;
import com.edu.pojo.Exercise;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author five-five
 * @created 2021/11/21 - 16:35
 */
public class Client01 {
    public static void main(String[] args) throws Exception {
        ExerciseBuilder exerciseBuilder = new ExerciseBuilder();
        File file = exerciseBuilder.startNormalExamination(1).exerciseToFile();
        System.out.println();
        List<String> exercises = exerciseBuilder.fileToExercise();
        List<Exercise> chooseProblem = exerciseBuilder.getChooseProblem(0);
        System.out.println();
    }
}
