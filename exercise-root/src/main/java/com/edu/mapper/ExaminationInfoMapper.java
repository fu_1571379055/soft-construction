package com.edu.mapper;

import com.edu.pojo.Examination;
import com.edu.pojo.ExaminationInfo;
import com.edu.pojo.ExaminationInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ExaminationInfoMapper {
    long countByExample(ExaminationInfoExample example);

    int deleteByExample(ExaminationInfoExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ExaminationInfo record);

    int insertSelective(ExaminationInfo record);

    List<ExaminationInfo> selectByExample(ExaminationInfoExample example);

    ExaminationInfo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ExaminationInfo record, @Param("example") ExaminationInfoExample example);

    int updateByExample(@Param("record") ExaminationInfo record, @Param("example") ExaminationInfoExample example);

    int updateByPrimaryKeySelective(ExaminationInfo record);

    int updateByPrimaryKey(ExaminationInfo record);

    int updateStateByUserAndRedisKeyAndExaminationId(ExaminationInfo examinationInfo);

    List<Examination> getFinishedExaminationByUserId(String userId);

    List<ExaminationInfo> selectByExamination(Examination examination);
}