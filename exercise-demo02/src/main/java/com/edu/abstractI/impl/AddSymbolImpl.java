package com.edu.abstractI.impl;

import com.edu.abstractI.Symbol;

/**
 * @author five-five
 * @created 2021/11/20 - 20:42
 */
public class AddSymbolImpl implements Symbol {
    @Override
    public String getSymbol() {
        return "+";
    }

    @Override
    public int calculate(int left, int right) {
        return left + right;
    }
}
