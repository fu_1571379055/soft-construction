package com.edu.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.edu.mapper.UserMapper;
import com.edu.pojo.RestResult;
import com.edu.pojo.StateEnum;
import com.edu.pojo.User;
import com.edu.pojo.UserExample;
import com.edu.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author five-five
 * @created 2021/11/12 - 9:10
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public RestResult addUserByUserNameAndPwd(User user) {
        //判断用户名是否已存在
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria().andUsernameEqualTo(user.getUsername());
        List<User> users = userMapper.selectByExample(userExample);
        if (users != null && users.size() > 0) {
            return RestResult.build(StateEnum.ERROR.getCode(), "用户名重复，请重新输入");
        }
        //垃圾回收
        criteria = null;
        userExample = null;
        int insert = -1;
        try {
            insert = userMapper.insert(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return insert < 0
                ? RestResult.build(StateEnum.ERROR.getCode(), StateEnum.ERROR.getText())
                : RestResult.build(StateEnum.SUCCESS.getCode(), StateEnum.SUCCESS.getText());
    }

    @Override
    public RestResult getUserByUserNameAndPwd(User user) {
        UserExample userExample = new UserExample();
        userExample.createCriteria()
                .andUsernameEqualTo(user.getUsername())
                .andUserpwdEqualTo(user.getUserpwd());
        List<User> users = userMapper.selectByExample(userExample);
        if (CollectionUtil.isEmpty(users)) {
            return RestResult.build(StateEnum.ERROR.getCode(), "登陆失败，无用户名" + user.getUsername());
        }
        User data = users.stream().findAny().get();
        RestResult restResult = RestResult.build(StateEnum.SUCCESS.getCode(), StateEnum.SUCCESS.getText());
        restResult.setData(data);
        return restResult;
    }

    @Override
    public RestResult editUserPwd(User user) {
        UserExample userExample = new UserExample();
        //根据用户名进行查询
        userExample.createCriteria()
                .andIdEqualTo(user.getId())
                .andUsernameEqualTo(user.getUsername());
        List<User> users = userMapper.selectByExample(userExample);
        if (CollectionUtil.isEmpty(users)) {
            return RestResult.build(StateEnum.ERROR.getCode(), "修改失败，无用户名" + user.getUsername());
        }
        //修改
        int i = -1;
        try {
            i = userMapper.updateByPrimaryKey(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i < 0
                ? RestResult.build(StateEnum.ERROR.getCode(), StateEnum.ERROR.getText())
                : RestResult.build(StateEnum.SUCCESS.getCode(), StateEnum.SUCCESS.getText());
    }
}
