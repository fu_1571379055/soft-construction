package com.edu;

import com.edu.service.impl.ExaminationServiceImpl;
import com.edu.service.impl.ExciseServiceImpl;
//import org.junit.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ImportResource;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author five-five
 * @created 2021/11/8-18:41
 */
@MapperScan(value = {"com.edu.mapper"})
@EnableSwagger2
@ImportResource({"classpath:spring/*.xml"})
@SpringBootApplication
public class ApplicationMainStarter {
    public static void main(String[] args) {
        //todo:配置事务管理器
        SpringApplication.run(ApplicationMainStarter.class, args);
    }

//    @Test
    public void test01() {
        AnnotationConfigApplicationContext configApplicationContext = new AnnotationConfigApplicationContext(ApplicationMainStarter.class);
        ExaminationServiceImpl examinationService = configApplicationContext.getBean(ExaminationServiceImpl.class);
        ExciseServiceImpl exciseService = configApplicationContext.getBean(ExciseServiceImpl.class);
    }
}
