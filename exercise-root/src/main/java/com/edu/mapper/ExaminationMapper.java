package com.edu.mapper;

import com.edu.pojo.Examination;
import com.edu.pojo.ExaminationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ExaminationMapper {
    long countByExample(ExaminationExample example);

    int deleteByExample(ExaminationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Examination record);

    int insertSelective(Examination record);

    List<Examination> selectByExample(ExaminationExample example);

    Examination selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Examination record, @Param("example") ExaminationExample example);

    int updateByExample(@Param("record") Examination record, @Param("example") ExaminationExample example);

    int updateByPrimaryKeySelective(Examination record);

    int updateByPrimaryKey(Examination record);

    List<Examination> selectByNameAndUserId(Examination examination);

    List<Examination> getExaminationByUserId(int userId);

}