package com.edu.abstractI;

/**
 * @author five-five
 * @created 2021/11/20 - 20:41
 */
public interface Symbol {
    //获取符号
    String getSymbol();
    //计算的方法
    int calculate(int left, int right);
}
