package com.edu.pojo;

/**
 * @author five-five
 * @created 2021/11/12 - 13:41
 */
public enum AllKeyEnum {
    ADD_BASES("add_base"),
    SUBTRACT_BASES("sub_base"),
    ADD_ADDEXCISES("add_addexcises"),
    SUBTRACT_SUBTRACTEXCISES("subtract_subtractexcises"),
    PREFIX_ADD_EXCISES("prefix_excises_add_excises_nice_"),
    PREFIX_SUBTRACT_EXCISES("prefix_excises_subtract__excises_nice_"),
    ADD_MAX_LENGTH("add_max_length"),
    SUBTRACT_MAX_LENGTH("subtract_max_length");

    private String key;

    AllKeyEnum(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
