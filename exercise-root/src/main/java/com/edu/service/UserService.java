package com.edu.service;

import com.edu.pojo.RestResult;
import com.edu.pojo.User;

/**
 * @author five-five
 * @created 2021/11/12 - 9:10
 */
public interface UserService {
    RestResult addUserByUserNameAndPwd(User user);

    RestResult getUserByUserNameAndPwd(User user);

    RestResult editUserPwd(User user);
}
