package com.edu.service;

import com.edu.pojo.Examination;
import com.edu.pojo.RestResult;

import javax.servlet.http.HttpServletRequest;

/**
 * @author five-five
 * @created 2021/11/12 - 15:32
 */
public interface ExaminationService {
     RestResult randomGenerateExamination(int length, String name, HttpServletRequest request);

    RestResult getExaminationByUserId(int userId);

    RestResult chooseAndDoIt(Examination examination, int kind);

    RestResult modifyExamination(String examinationStr, HttpServletRequest request);

    RestResult getFinishedExamination(String userId);
}
