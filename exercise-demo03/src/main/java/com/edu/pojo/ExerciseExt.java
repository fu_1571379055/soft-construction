package com.edu.pojo;

/**
 * @author five-five
 * @created 2021/11/26-17:42
 */
public class ExerciseExt extends Exercise {
    private boolean correct;

    public ExerciseExt() {
    }

    public ExerciseExt(int left, int right, String symbol, int result) {
        super(left, right, symbol, result);
    }

    public ExerciseExt(int left, int right, String symbol, int result, boolean correct) {
        super(left, right, symbol, result);
        this.correct = correct;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }
}
