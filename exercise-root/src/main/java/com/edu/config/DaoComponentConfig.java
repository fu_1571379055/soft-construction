package com.edu.config;

import com.alibaba.druid.support.http.StatViewServlet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.HashMap;

/**
 * @作者 five-five
 * @创建时间 2021/1/13
 */
@Configuration
@PropertySource(value = {"classpath:/daoController.properties"})
public class DaoComponentConfig {
    //Druid后台界面访问参数
    @Value("${druid_loginUsername}")
    private final String DRUID_LOGING_USERNAME = null;
    @Value("${druid_loginPassword}")
    private final String DRUID_LOGING_PASSWORD = null;
    @Value("${druid_allow}")
    private final String DRUID_LOGING_ALLOW = null;

    /**
     * 用于处理Druid监视见面的servlet
     *
     * @return
     */
    @Bean
    //有这个servlet的话就不注入了
    @ConditionalOnMissingBean
    public ServletRegistrationBean statViewServlet() {
        //拦截路径
        ServletRegistrationBean<StatViewServlet> bean = new ServletRegistrationBean<>(new StatViewServlet(), "/druid/*");
        HashMap<String, String> initParameters = new HashMap<>();
        // 增加配置项
        initParameters.put("loginUsername", DRUID_LOGING_USERNAME);
        initParameters.put("loginPassword", DRUID_LOGING_PASSWORD);
//        initParameters.put("allow", DRUID_LOGING_ALLOW);     //如果为空所有人都可以访问，如果localhost本机可以访问，如果具体的ip值则具体的值
        // 关于其他配置可以在类ResourceServlet下查看
        // 禁止xu访问       initParameters.put("xu","192.168.1.**");
        // 后台需要有人登陆
        bean.setInitParameters(initParameters);
        return bean;
    }
}
