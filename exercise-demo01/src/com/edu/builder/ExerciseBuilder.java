package com.edu.builder;

import com.edu.abstractI.ExerciseGenerate;
import com.edu.abstractI.impl.*;
import com.edu.pojo.Exercise;

import java.util.ArrayList;

/**
 * @author five-five
 * @created 2021/11/21 - 15:57
 */
public class ExerciseBuilder {
    private ExerciseGenerate subtractGenerate = new SubtractExerciseGenerate(new SubtractSymbolImpl());
    private ExerciseGenerate addGenerate = new AddExerciseGenerate(new AddSymbolImpl());
    private ExerciseGenerate binaryGenerate = new BinaryExerciseGenerate();
    private ArrayList<Exercise> exerciseArrayList = new ArrayList<>();

    public ExerciseBuilder generateAdd(int length) {
        exerciseArrayList.addAll(addGenerate.generateExercise(length));
        return this;
    }
    public ExerciseBuilder generateSubtract(int length) {
        exerciseArrayList.addAll(subtractGenerate.generateExercise(length));
        return this;
    }
    public ExerciseBuilder generateBinary(int length) {
        exerciseArrayList.addAll(binaryGenerate.generateExercise(length));
        return this;
    }

    public ArrayList<Exercise> getExerciseArrayList() {
        return exerciseArrayList;
    }
}
