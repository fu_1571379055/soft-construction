package com.edu.pojo;

import java.util.List;

/**
 * @author five-five
 * @created 2021/11/26-15:46
 */
public class Examination {
    private String username;
    private String key;
    private List<ExerciseExt> exerciseExts;
    public Examination() {
    }

    public Examination(String userid, String id, List<ExerciseExt> exerciseExts) {
        this.username = userid;
        this.key = id;
        this.exerciseExts = exerciseExts;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<ExerciseExt> getExerciseExts() {
        return exerciseExts;
    }

    public void setExerciseExts(List<ExerciseExt> exerciseExts) {
        this.exerciseExts = exerciseExts;
    }
}
