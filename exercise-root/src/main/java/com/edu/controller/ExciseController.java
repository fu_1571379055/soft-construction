package com.edu.controller;

import com.edu.pojo.Excise;
import com.edu.pojo.AllKeyEnum;
import com.edu.pojo.RestResult;
import com.edu.pojo.StateEnum;
import com.edu.service.ExciseService;
import com.edu.util.JsonUtil;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author five-five
 * @created 2021/11/12 - 12:17
 */
@Api(tags = "习题类相关接口", value = "习题类相关接口")
@RestController
@CrossOrigin
public class ExciseController {
    @Resource
    private ExciseService exciseService;

    @ApiOperation("生成标椎加算式基")
    @RequestMapping(value = "/generateAddFormulaBase", method = RequestMethod.GET)
    public RestResult generateAddFormulaBase(HttpServletRequest request) {
        Excise[][] excises = exciseService.generateAddFormulaBase(request);
        //保存至会话
        request.getSession().setAttribute(AllKeyEnum.ADD_BASES.getKey(), JsonUtil.objectToJson(excises));
        RestResult restResult = RestResult.build(StateEnum.SUCCESS.getCode(), StateEnum.SUCCESS.getText(), excises);
        return restResult;
    }

    @ApiOperation("生成标椎减算式基")
    @RequestMapping(value = "/generateSubtractFormulaBase", method = RequestMethod.GET)
    public RestResult generateSubtractFormulaBase(HttpServletRequest request) {
        Excise[][] excises = exciseService.generateSubtractFormulaBase(request);
        //保存至会话
        request.getSession().setAttribute(AllKeyEnum.SUBTRACT_BASES.getKey(), JsonUtil.objectToJson(excises));
        RestResult restResult = RestResult.build(StateEnum.SUCCESS.getCode(), StateEnum.SUCCESS.getText(), excises);
        return restResult;
    }

    @ApiOperation("基于加算式基生成算式")
    @RequestMapping(value = "/generateAddExcises", method = RequestMethod.GET)
    @ApiImplicitParam(name = "length", value = "题目数量")
    public RestResult generateAddExcises(int length, HttpServletRequest request) {
        List<Excise> exciseList = exciseService.generateAddExcises(length, request);
        RestResult restResult = RestResult.build(StateEnum.SUCCESS.getCode(), StateEnum.SUCCESS.getText(), exciseList);
        return restResult;
    }

    @ApiOperation("基于减算式基生成算式")
    @RequestMapping(value = "/generateSubtractExcises", method = RequestMethod.GET)
    @ApiImplicitParam(name = "length", value = "题目数量")
    public RestResult generateSubtractExcises(
            int length, HttpServletRequest request) {
        List<Excise> exciseList = exciseService.generateSubtractExcises(length, request);
        RestResult restResult = RestResult.build(StateEnum.SUCCESS.getCode(), StateEnum.SUCCESS.getText(),exciseList);
        return restResult;
    }

    @ApiOperation("基于算式基随机生成算式")
    @RequestMapping(value = "/randomGenerateSubtractExcises", method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "maxlength", value = "算式数量", required = true),
            @ApiImplicitParam(name = "length", value = "加算式数量", required = true),
    })
    public RestResult randomGenerateSubtractExcises(
            int maxlength, int length, HttpServletRequest request) {
        Map<String, List<Excise>> map = exciseService.randomGenerateSubtractExcises(maxlength, length, request);
        RestResult restResult = RestResult.build(StateEnum.SUCCESS.getCode(), StateEnum.SUCCESS.getText(), map);
        return restResult;
    }
}
