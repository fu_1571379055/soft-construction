package com.edu.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

/**
 * t-excise
 * @author 
 */
@ApiModel(value="试题类(保存算式)")
public class Excise implements Serializable {
    /**
     * 主键-自增
     */
    @ApiModelProperty(value="主键-自增")
    private Integer id;

    /**
     * 左操作数
     */
    @ApiModelProperty(value="左操作数")
    private String left;

    /**
     * 右操作数
     */
    @ApiModelProperty(value="右操作数")
    private String right;

    /**
     * 符号
     */
    @ApiModelProperty(value="符号")
    private String symbol;

    /**
     * 结果
     */
    @ApiModelProperty(value="结果")
    private String result;
    /**
     * redis-key
     */
    @ApiModelProperty(value="唯一标识")
    private String redisKey;

    public String getRedisKey() {
        return redisKey;
    }

    public void setRedisKey(String redisKey) {
        this.redisKey = redisKey;
    }

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Excise other = (Excise) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getLeft() == null ? other.getLeft() == null : this.getLeft().equals(other.getLeft()))
            && (this.getRight() == null ? other.getRight() == null : this.getRight().equals(other.getRight()))
            && (this.getSymbol() == null ? other.getSymbol() == null : this.getSymbol().equals(other.getSymbol()))
            && (this.getResult() == null ? other.getResult() == null : this.getResult().equals(other.getResult()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getLeft() == null) ? 0 : getLeft().hashCode());
        result = prime * result + ((getRight() == null) ? 0 : getRight().hashCode());
        result = prime * result + ((getSymbol() == null) ? 0 : getSymbol().hashCode());
        result = prime * result + ((getResult() == null) ? 0 : getResult().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", left=").append(left);
        sb.append(", right=").append(right);
        sb.append(", symbol=").append(symbol);
        sb.append(", result=").append(result);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}