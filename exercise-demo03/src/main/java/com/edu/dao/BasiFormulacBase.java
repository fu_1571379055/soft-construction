package com.edu.dao;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import com.edu.pojo.NormalKeys;
import com.edu.util.JsonUtil;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * 算式基
 *
 * @author five-five
 * @created 2021/11/26-17:20
 */
public class BasiFormulacBase {
    private BasiFormulacBase() {
    }

    /**
     * 加算式基
     *
     * @return
     */
    public static int[][] generateAddBasiFormulacBase() {
        int[][] bases = null;
        if ((bases = filetoBasicbases(NormalKeys.ADD_BASICBASEFILEPATH)) != null) {
            return bases;
        }
        bases = new int[101][101];
        for (int i = 0; i < bases.length; i++) {
            for (int j = 0; j < bases[i].length; j++) {
                if (j + i < 100) {
                    bases[i][j] = i + j;
                }
            }
        }
        //持久化至文件
        saveDataToFile(JsonUtil.objectToJson(bases), NormalKeys.ADD_BASICBASEFILEPATH);
        return bases;
    }

    /**
     * 减算式基
     *
     * @return
     */
    public static int[][] generateSubtractBasiFormulacBase() {
        int[][] bases = null;
        if ((bases = filetoBasicbases(NormalKeys.SUBTRACT_BASICBASEFILEPATH)) != null) {
            return bases;
        }
        bases = new int[101][101];
        for (int i = 0; i < bases.length; i++) {
            for (int j = 0; j < bases[i].length; j++) {
                if (j - i > 0 && j - i < 100) {
                    bases[i][j] = j - i;
                }
            }
        }
        //持久化至文件
        saveDataToFile(JsonUtil.objectToJson(bases), NormalKeys.SUBTRACT_BASICBASEFILEPATH);
        return bases;
    }

    /**
     * 保存数据至文件
     *
     * @param dataStr  json数据字符
     * @param filepath 文件路径
     */
    public static void saveDataToFile(String dataStr, String filepath) {
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(filepath));
            byte[] bytes = dataStr.getBytes(StandardCharsets.UTF_8);
            bos.write(bytes, 0, bytes.length);
            bos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 根据文件路径读取数据,并返回算式基
     *
     * @param filepath
     * @return
     */
    public static int[][] filetoBasicbases(String filepath) {
        int[][] bases = null;
        byte[] bytes = new byte[1024 * 1024];
        int len = -1;
        BufferedInputStream bis = null;
        StringBuilder sb = new StringBuilder();
        try {
            bis = new BufferedInputStream(new FileInputStream(filepath));
            while ((len = bis.read(bytes)) != -1) {
                sb.append(new String(bytes));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (StrUtil.isBlank(sb.toString())) {
            return bases;
        }
        bases = JsonUtil.jsonToPojo(sb.toString(), int[][].class);
        return bases;
    }

}
