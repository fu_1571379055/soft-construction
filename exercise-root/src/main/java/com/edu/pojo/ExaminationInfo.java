package com.edu.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * t_examinationInfo
 *
 * @author
 */
@ApiModel(value = "com.edu.pojo.ExaminationInfo")
public class ExaminationInfo implements Serializable {
    /**
     * 主键-自增
     */
    @ApiModelProperty(value = "主键-自增")
    private Integer id;

    /**
     * 试题唯一标识
     */
    @ApiModelProperty(value = "试题唯一标识")
    private Integer exciseId;

    /**
     * 试卷唯一标识
     */
    @ApiModelProperty(value = "试卷唯一标识")
    private Integer examinationId;

    /**
     * 用户唯一标识
     */
    @ApiModelProperty(value = "用户唯一标识")
    private Integer userId;

    /**
     * 试卷对应题的状态(0未做；1已做；2已做错；3已做对)
     */
    @ApiModelProperty(value = "试卷对应题的状态(0未做；1已做；2已做错；3已做对)")
    private Integer status;

    /**
     * 对应的redis的key值
     */
    @ApiModelProperty(value = "对应的redis的key值")
    private String redisKey;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getExciseId() {
        return exciseId;
    }

    public void setExciseId(Integer exciseId) {
        this.exciseId = exciseId;
    }

    public Integer getExaminationId() {
        return examinationId;
    }

    public void setExaminationId(Integer examinationId) {
        this.examinationId = examinationId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRedisKey() {
        return redisKey;
    }

    public void setRedisKey(String redisKey) {
        this.redisKey = redisKey;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ExaminationInfo other = (ExaminationInfo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getExciseId() == null ? other.getExciseId() == null : this.getExciseId().equals(other.getExciseId()))
                && (this.getExaminationId() == null ? other.getExaminationId() == null : this.getExaminationId().equals(other.getExaminationId()))
                && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
                && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
                && (this.getRedisKey() == null ? other.getRedisKey() == null : this.getRedisKey().equals(other.getRedisKey()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getExciseId() == null) ? 0 : getExciseId().hashCode());
        result = prime * result + ((getExaminationId() == null) ? 0 : getExaminationId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getRedisKey() == null) ? 0 : getRedisKey().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", exciseId=").append(exciseId);
        sb.append(", examinationId=").append(examinationId);
        sb.append(", userId=").append(userId);
        sb.append(", status=").append(status);
        sb.append(", redisKey=").append(redisKey);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}