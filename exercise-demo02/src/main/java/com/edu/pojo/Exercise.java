package com.edu.pojo;

import com.edu.abstractI.Symbol;
import com.edu.abstractI.impl.AddSymbolImpl;

import java.util.Objects;

/**
 * @author five-five
 * @created 2021/11/20 - 20:39
 */
public class Exercise {
    //左操作数
    private int left = 0;
    //右操作数
    private int right = 0;
    //符号,默认为加法运算
    private Symbol symbol = new AddSymbolImpl();
    //结果
    private int result = symbol.calculate(left, right);

    public Symbol getSymbol() {
        return symbol;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
        this.result= symbol.calculate(this.left,this.right);
    }

    public void setResult(int result) {
        this.result = result;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public Exercise() {
    }

    public int getResult() {
        return result;
    }

    public Exercise(int left, int right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exercise exercise = (Exercise) o;
        return left == exercise.left && right == exercise.right && result == exercise.result && Objects.equals(symbol, exercise.symbol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right, symbol, result);
    }

    @Override
    public String toString() {
        return "Exercise{" +
                "left=" + left +
                ", right=" + right +
                ", symbol=" + symbol +
                ", result=" + result +
                '}';
    }
}
