package com.edu.abstractI;

/**
 * 序列化时：无法构造`com.edu.abstractI.Symbol` 的实例（不存在像默认构造函数这样的创建者）：
 * 抽象类型要么需要映射到具体类型，
 * 要么具有自定义反序列化器，
 * 要么包含其他类型信息
 *
 * @author five-five
 * @created 2021/11/20 - 20:41
 */
public interface Symbol {
    //获取符号
    String getSymbol();

    //计算的方法
    int calculate(int left, int right);
}
