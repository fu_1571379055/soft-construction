package com.edu.util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * 实例：
 *      JedisPoolUtil.DATA_BASE = 2;
 *      JedisPoolUtil.getJedisPoolInstance();
 *      String set = JedisPoolUtil.set("wong", "666");
 *      System.out.println(set);
 */
public class JedisPoolUtil {
    /**
     * 创建JedisPool实例
     */
    private static volatile JedisPool pool = null;
    //Redis服务器IP
//    private static String ADDR = "39.104.54.209";
    private static String ADDR = "192.168.137.11";

    //Redis的端口号
    private static int PORT = 6379;

    //访问密码
    private static String AUTH = null;

    //可用连接实例的最大数目，默认值为8；
    //如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
    private static int MAX_ACTIVE = 1024;

    //控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值8。
    private static int MAX_IDLE = 200;

    //等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException；
    private static int MAX_WAIT = 10000;

    private static int TIMEOUT = 10000;

    //在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
    private static boolean TEST_ON_BORROW = true;
    //
    private static int DATA_BASE = 0;

    /**
     * 私有化构造方法JedisPoolUtil.
     */
    private JedisPoolUtil() {
    }

    /**
     * @return JedisPool  返回类型
     * @throws
     * @Title: getJedisPoolInstance
     * @Description: TODO(JedisPool的连接池)
     */
    public static JedisPool getJedisPoolInstance() {

        if (pool == null) {
            synchronized (JedisPoolUtil.class) {
                if (pool == null) {
                    JedisPoolConfig poolConfig = new JedisPoolConfig();

                    poolConfig.setMaxActive(MAX_ACTIVE);

                    poolConfig.setMaxIdle(MAX_IDLE);

                    poolConfig.setMaxWait(MAX_WAIT);

                    poolConfig.setTestOnBorrow(TEST_ON_BORROW);

                    pool = new JedisPool(poolConfig, ADDR, PORT, TIMEOUT, AUTH, DATA_BASE);

                }
            }
        }
        return pool;
    }

    /**
     * @param pool
     * @param jedis void  返回类型
     * @throws
     * @Title: release
     * @Description: TODO(回收jedis)
     */
    public static void release(JedisPool pool, Jedis jedis) {
        if (jedis != null) {
            pool.returnResourceObject(jedis);
        }
    }

    /**
     * @param key   指定的键
     * @param value 指定的值
     * @return String  返回类型
     * @throws
     * @Title: set
     * @Description: TODO(设置一个值向Redis)
     */
    public static String set(String key, String value) {
        // TODO Auto-generated method stub
        Jedis jedis = null;
        String set = null;
        try {
            jedis = pool.getResource();
            set = jedis.set(key, value);

        } catch (Exception e) {
            // TODO: handle exception
            pool.returnBrokenResource(jedis);
            e.printStackTrace();
        } finally {
            release(pool, jedis);
        }
        return set;

    }

    /**
     * @param key     指定的键
     * @param value   指定的值
     * @param dbIndex 数据库下标
     * @return String  返回类型
     * @throws
     * @Title: set
     * @Description: TODO(设置一个值向Redis 可以选择数据库)
     */
    public static String set(String key, String value, int dbIndex) {
        // TODO Auto-generated method stub
        Jedis jedis = null;
        String set = null;
        try {
            jedis = pool.getResource();
            jedis.select(dbIndex);
            set = jedis.set(key, value);

        } catch (Exception e) {
            // TODO: handle exception
            pool.returnBrokenResource(jedis);
            e.printStackTrace();
        } finally {
            release(pool, jedis);
        }
        return set;

    }

    /**
     * @param key 指定的键
     * @return String  返回类型
     * @throws
     * @Title: get
     * @Description: TODO(获取一个值)
     */
    public static String get(String key) {
        // TODO Auto-generated method stub
        Jedis jedis = null;
        String set = null;
        try {
            jedis = pool.getResource();
            set = jedis.get(key);
        } catch (Exception e) {
            // TODO: handle exception
            pool.returnBrokenResource(jedis);
            e.printStackTrace();
        } finally {
            release(pool, jedis);
        }
        return set;
    }

    /**
     * @param key     指定的键
     * @param dbIndex 数据库下标
     * @return String  返回类型
     * @throws
     * @Title: get
     * @Description: TODO(获取一个值 可以选择数据库)
     */
    public static String get(String key, int dbIndex) {
        // TODO Auto-generated method stub
        Jedis jedis = null;
        String set = null;
        try {
            jedis = pool.getResource();
            jedis.select(dbIndex);
            set = jedis.get(key);
        } catch (Exception e) {
            // TODO: handle exception
            pool.returnBrokenResource(jedis);
            e.printStackTrace();
        } finally {
            release(pool, jedis);
        }
        return set;
    }

    /**
     * @param key     指定的键
     * @param seconds 设置的秒数
     * @return Long  返回类型
     * @throws
     * @Title: setOutTime
     * @Description: TODO(给指定键设置一个过期时间)
     */
    public static Long setOutTime(String key, int seconds) {
        // TODO Auto-generated method stub
        Jedis jedis = null;
        Long set = null;
        try {
            jedis = pool.getResource();
            set = jedis.expire(key, seconds);
        } catch (Exception e) {
            // TODO: handle exception
            pool.returnBrokenResource(jedis);
            e.printStackTrace();
        } finally {
            release(pool, jedis);
        }
        return set;
    }

    /**
     * @param key     指定的键
     * @param seconds 设置秒数
     * @param dbIndex 数据库下标
     * @return Long  返回类型
     * @throws
     * @Title: setOutTime
     * @Description: TODO(给指定键设置一个过期时间 可以选择数据库)
     */
    public static Long setOutTime(String key, int seconds, int dbIndex) {
        // TODO Auto-generated method stub
        Jedis jedis = null;
        Long set = null;
        try {
            jedis = pool.getResource();
            jedis.select(dbIndex);
            set = jedis.expire(key, seconds);
        } catch (Exception e) {
            // TODO: handle exception
            pool.returnBrokenResource(jedis);
            e.printStackTrace();
        } finally {
            release(pool, jedis);
        }
        return set;
    }

    /**
     * @param key 指定的键
     * @return Boolean  返回类型
     * @throws
     * @Title: exists
     * @Description: TODO(判断指定键是否存在)
     */
    public static Boolean exists(String key) {
        // TODO Auto-generated method stub
        Jedis jedis = null;
        Boolean set = null;
        try {
            jedis = pool.getResource();
            set = jedis.exists(key);
        } catch (Exception e) {
            // TODO: handle exception
            pool.returnBrokenResource(jedis);
            e.printStackTrace();
        } finally {
            release(pool, jedis);
        }
        return set;
    }

    /**
     * @param key     指定得键
     * @param dbIndex 数据库下标
     * @return Boolean  返回类型
     * @throws
     * @Title: exists
     * @Description: TODO(判断指定键是否存在 可以选择数据库)
     */
    public static Boolean exists(String key, int dbIndex) {
        // TODO Auto-generated method stub
        Jedis jedis = null;
        Boolean set = null;
        try {
            jedis = pool.getResource();
            jedis.select(dbIndex);
            set = jedis.exists(key);
        } catch (Exception e) {
            // TODO: handle exception
            pool.returnBrokenResource(jedis);
            e.printStackTrace();
        } finally {
            release(pool, jedis);
        }
        return set;
    }

    /**
     * @param keys 要删除值的名称 这里是个字符串无限参数
     * @return Long  返回类型
     * @throws
     * @Title: del
     * @Description: TODO(删除指定的值)
     */
    public static Long del(String... keys) {
        // TODO Auto-generated method stub
        Jedis jedis = null;
        Long set = null;
        try {
            jedis = pool.getResource();
            set = jedis.del(keys);
        } catch (Exception e) {
            // TODO: handle exception
            pool.returnBrokenResource(jedis);
            e.printStackTrace();
        } finally {
            release(pool, jedis);
        }
        return set;
    }

    /**
     * @param dbIndex 数据库下标
     * @param keys    要删除值的名称 这里是个字符串无限参数
     * @return Long  返回类型
     * @throws
     * @Title: del
     * @Description: TODO(删除指定的值 可以选择数据库)
     */
    public static Long del(int dbIndex, String... keys) {
        // TODO Auto-generated method stub
        Jedis jedis = null;
        Long set = null;
        try {
            jedis = pool.getResource();
            jedis.select(dbIndex);
            set = jedis.del(keys);
        } catch (Exception e) {
            // TODO: handle exception
            pool.returnBrokenResource(jedis);
            e.printStackTrace();
        } finally {
            release(pool, jedis);
        }
        return set;
    }

    /**
     * @param upass void  返回类型
     * @throws
     * @Title: setAUTH
     * @Description: TODO(设置密码默认为 null < p > 如果没有密码请不要设置 < / p >)
     */
    public static void setPassword(String upass) {
        AUTH = upass;
    }

    /**
     * @param dbIndex void  返回类型
     * @throws
     * @Title: setDATA_BASE
     * @Description: TODO(选择数据库 默认为0号数据库)
     */
    public static void setdataBase(int dbIndex) {
        DATA_BASE = dbIndex;
    }

    /*
     * 测试
     */
    public static void main(String[] args) {
        JedisPoolUtil.DATA_BASE = 2;
        JedisPoolUtil.getJedisPoolInstance();
        String set = JedisPoolUtil.set("wong", "666");
        System.out.println(set);
    }
}