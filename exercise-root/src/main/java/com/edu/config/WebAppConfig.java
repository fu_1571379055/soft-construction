package com.edu.config;

import com.fasterxml.classmate.TypeResolver;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.Ordered;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.schema.AlternateTypeRules;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 所有关于页面的整合都在这里
 *
 * @作者 five-five
 * @创建时间 2021/1/11
 */
@Configuration
public class WebAppConfig implements WebMvcConfigurer {
    @Resource
    private TypeResolver typeResolver;

    /**
     * 视图解析器，不想写后缀.html
     *
     * @return
     */
    public InternalResourceViewResolver defaultViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/");
        resolver.setSuffix("html");
        return resolver;
    }


    /**
     * 整合Swagger文档
     *
     * @return
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)//配置了Swagger的Docket的bean实例
                .pathMapping("/")
                .enable(true)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.edu.controller"))//配置扫描那个包
//                .apis(RequestHandlerSelectors.withMethodAnnotation(RequestMapping.class))
//                .paths(PathSelectors.ant("/stu/**"))
                .paths(PathSelectors.any())
                .build().apiInfo(new ApiInfoBuilder()
                        .title("欢迎来到Swagger调试界面")
                        .description("软件构造作业，详细信息......")
                        .version("1.0")
                        .contact(new Contact("five-five", "https://blog.csdn.net/qq_45074341", "1571379055@qq.com"))
                        .license("The Apache License")
                        .licenseUrl("https://blog.csdn.net/qq_45074341")
                        .termsOfServiceUrl("five-five的软件构造作业")
                        .extensions(new ArrayList<>())
                        .build());
    }
}