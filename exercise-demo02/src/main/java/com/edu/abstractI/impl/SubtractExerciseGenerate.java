package com.edu.abstractI.impl;

import com.edu.abstractI.ExerciseGenerate;
import com.edu.abstractI.Symbol;
import com.edu.enums.ExercisesBase;
import com.edu.pojo.Exercise;

import java.util.ArrayList;
import java.util.List;

/**
 * 生成减算式
 *
 * @author five-five
 * @created 2021/11/20 - 21:10
 */
public class SubtractExerciseGenerate extends ExerciseGenerate {

    public SubtractExerciseGenerate(Symbol symbol) {
        super(symbol);
    }

    public SubtractExerciseGenerate() {
    }

    @Override
    public List<Exercise> generateExercise(int length) {
        ArrayList<Exercise> exercises = new ArrayList<>();
        while (exercises.size() < length) {
            int x = (int) Math.ceil(Math.random() * ExercisesBase.len) - 1;
            int y = (int) Math.ceil(Math.random() * ExercisesBase.len) - 1;
            Exercise[][] exercisesBaseBaseToList = ExercisesBase.getExercisesBaseBaseToArray(ExercisesBase.getSubtractExercisesBase(), symbol);
            Exercise exercise = exercisesBaseBaseToList[x][y];
            if (exercise.getResult() > 0 && exercise.getResult() < 100 && !exercises.contains(exercise)) {
                exercises.add(exercise);
            }
        }
        return exercises;
    }
}
