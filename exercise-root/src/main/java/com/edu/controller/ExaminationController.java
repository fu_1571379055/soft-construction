package com.edu.controller;

import com.edu.pojo.Examination;
import com.edu.pojo.ExaminationVo;
import com.edu.pojo.RestResult;
import com.edu.pojo.StateEnum;
import com.edu.service.ExaminationInfoService;
import com.edu.service.ExaminationService;
import io.swagger.annotations.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author five-five
 * @created 2021/11/12 - 15:31
 */
@Api(tags = "试卷类相关接口")
@RestController
@SessionAttributes({"examination"})//未使用到Model此注解失效
@CrossOrigin
public class ExaminationController {
    @Resource
    private ExaminationService examinationService;
    @Resource
    private ExaminationInfoService examinationInfoService;

    @ApiOperation(value = "随机生成一份试卷")
    @RequestMapping(value = "/randomGenerateExamination", method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "length", value = "题目数量"),
            @ApiImplicitParam(name = "name", value = "试卷名称")
    })
    public RestResult randomGenerateExamination(int length, String name, HttpServletRequest request) {
        if (length < 0) {
            return RestResult.build(StateEnum.ERROR.getCode(), "请再次输入选择题目数量");
        }
        if (StringUtils.isBlank(name)) {
            return RestResult.build(StateEnum.ERROR.getCode(), "请输入合法的试卷名称");
        }
        return examinationService.randomGenerateExamination(length, name, request);
    }

    @ApiOperation(value = "查询用户名下的试卷")
    @RequestMapping(value = "/getExaminationByUserId", method = RequestMethod.GET)
    @ApiImplicitParam(name = "userId")
    public RestResult getExaminationByUserId(int userId) {
        if (userId < 0) {
            return RestResult.build(StateEnum.ERROR.getCode(), "违法参数");
        }
        return examinationService.getExaminationByUserId(userId);
    }

    /**
     * 将查询到的Examination对象保存至会话中
     * todo:修改方法，增加filter功能
     *
     * @param examination 试卷
     * @return 返回试卷对应的试题
     */
    @ApiOperation(value = "选择试卷并做题，根据传递的kind来选择正确/错误的题目")
    @RequestMapping(value = "/chooseAndDoIt", method = RequestMethod.POST)
    @ApiImplicitParam(name = "kind", value = "题目类型,0:全部展示/1:展示错题/2:展示正确", required = true, defaultValue = "0")
    public RestResult chooseAndDoIt(
            @ApiParam(name = "examination", required = true, value = "试卷查询条件")
            @RequestBody Examination examination,
            @RequestParam(required = false, defaultValue = "0") int kind,
            HttpServletRequest request) {
        //判断内容是否填充完毕：id
        if (examination == null || examination.getId() <= 0) {
            return RestResult.build(StateEnum.ERROR.getCode(), StateEnum.ERROR.getText());
        }
        //todo：将此次查询条件保存至会话,返回改试卷下的所有题目
        request.getSession().setAttribute("examination", examination);
        return examinationService.chooseAndDoIt(examination, kind);
    }

    @ApiOperation(value = "修改试卷")
    @RequestMapping(value = "/modifyExamination", method = RequestMethod.POST)
    public RestResult modifyExamination(
            @ApiParam(name = "exciseStr", required = true, value = "答卷详情")
            @RequestBody String exciseStr, HttpServletRequest request) {
        if (StringUtils.isBlank(exciseStr)) {
            return RestResult.build(StateEnum.ERROR.getCode(), "参数不合法，请重新校验参数的正确性");
        }
        return examinationService.modifyExamination(exciseStr, request);
    }

    @ApiOperation(value = "查看自己做过的试卷，并返回ExaminationVo对象")
    @RequestMapping(value = "/getFinishedExamination", method = RequestMethod.GET)
    @ApiImplicitParam(name = "userId", value = "用户id", required = true)
    public RestResult getFinishedExamination(
            String userId) {
        if (StringUtils.isBlank(userId)) {
            return RestResult.build(StateEnum.ERROR.getCode(), "参数不合法，请重新校验参数的正确性");
        }
        return examinationService.getFinishedExamination(userId);
    }
}
