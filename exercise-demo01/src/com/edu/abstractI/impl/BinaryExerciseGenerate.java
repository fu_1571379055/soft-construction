package com.edu.abstractI.impl;

import com.edu.abstractI.ExerciseGenerate;
import com.edu.abstractI.Symbol;
import com.edu.pojo.Exercise;

import java.util.ArrayList;
import java.util.List;

/**
 * 产生混合算式
 *
 * @author five-five
 * @created 2021/11/21 - 15:40
 */
public class BinaryExerciseGenerate extends ExerciseGenerate{

    /**
     *
     * @param length 算式长度：加算式长度+减算式长度
     * @return 算式习题
     */
    @Override
    public List<Exercise> generateExercise(int length) {
        int mid = length / 2;
        ExerciseGenerate addGenerate = new AddExerciseGenerate(new AddSymbolImpl());
        ExerciseGenerate subtractGenerate = new SubtractExerciseGenerate(new SubtractSymbolImpl());
        ArrayList<Exercise> results = new ArrayList<>(length);
        results.addAll(addGenerate.generateExercise(mid));
        results.addAll(subtractGenerate.generateExercise(length - mid));
        return results;
    }
}
